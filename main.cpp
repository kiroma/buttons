#include <SFML/Graphics/Image.hpp>
#include <SFML/Window.hpp>
#include <array>
#include <filesystem>
#include <functional>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <memory>
#include <pgl.hpp>
#include <string>
#include <string_view>
#include <vector>

class drawable
{
protected:
	pgl::vao vao;
	pgl::storage::vbo vbo;
	pgl::storage::ebo ebo;
	GLsizei elements = 0;
	virtual pgl::Program& getProgram() = 0;

public:
	glm::vec2 pos;
	glm::vec2 size;
	drawable(glm::vec2 pos, glm::vec2 size) :
		pos(pos), size(size)
	{
	}
	virtual void draw()
	{
		getProgram().use();
		vao.bind();
		glDrawElements(GL_TRIANGLES, elements, GL_UNSIGNED_INT, nullptr);
	}
	virtual ~drawable() = default;
};

class interactable : public drawable
{
public:
	bool hovered = false;
	bool clicked = false;
	interactable(glm::vec2 pos, glm::vec2 size) :
		drawable(pos, size)
	{
	}
	virtual void onHover() = 0;
	virtual void onLeave() = 0;
	virtual void onClick() = 0;
	virtual void onRelease() = 0;
	template <class Point>
	bool containsPoint(Point point) const
	{
		if((point.x > pos.x) && (point.x < (pos.x + size.x)))
		{
			if((point.y > pos.y) && (point.y < (pos.y + size.y)))
			{
				return true;
			}
		}
		return false;
	}
};

class button final : public interactable
{
	pgl::Program& getProgram() override
	{
		static pgl::Program shaderProgram{pgl::ProgramBuilder().attach(pgl::Shader(GL_VERTEX_SHADER, "res/basic.vert")).attach(pgl::Shader(GL_FRAGMENT_SHADER, "res/basic.frag")).link()};
		return shaderProgram;
	}

	glm::vec4 color{1, 0, 1, 1};
	static constexpr std::array squareVertices =
	{glm::vec2{0, 0}, glm::vec2{1, 0},
	 glm::vec2{0, 1}, glm::vec2{1, 1}};
	static constexpr std::array<unsigned int, 6> squareIndices =
	{0, 1, 2,
	 2, 3, 1};
	std::function<void()> buttonFunc;

public:
	button(
	glm::vec2 pos, glm::vec2 size, std::function<void()>&& buttonFunc = []() {}) :
		interactable(pos, size), buttonFunc(std::forward<std::function<void()>>(buttonFunc))
	{
		vao.bind();
		vao.bindEBO(ebo);
		vbo.bufferData(squareVertices, 0);
		ebo.bufferData(squareIndices, 0);
		getProgram().setAttrib<GLfloat>(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
		elements = squareIndices.size();
	}
	void draw() override
	{
		getProgram().use();
		glUniform4fv(1, 1, glm::value_ptr(color));
		glm::mat4 model(1);
		model = glm::translate(model, glm::vec3(pos, 0));
		model = glm::scale(model, glm::vec3(size, 0));
		glUniformMatrix4fv(0, 1, GL_FALSE, glm::value_ptr(model));
		drawable::draw();
	}
	void onHover() override
	{
		color = {0, 1, 1, 1};
	}
	void onLeave() override
	{
		color = {1, 0, 1, 1};
	}
	void onClick() override
	{
		buttonFunc();
		color = {1, 0, 0, 1};
	}
	void onRelease() override
	{
		if(hovered)
			onHover();
		else
			onLeave();
	}
};

class picture : public drawable
{
	static constexpr std::array squareVertices =
	{glm::vec2{0, 0}, glm::vec2{1, 0},
	 glm::vec2{0, 1}, glm::vec2{1, 1}};
	static constexpr std::array<unsigned int, 6> squareIndices =
	{0, 1, 2,
	 2, 3, 1};
	pgl::Texture tex;
	pgl::Program& getProgram() override
	{
		static pgl::Program shaderProgram{pgl::ProgramBuilder().attach(pgl::Shader(GL_VERTEX_SHADER, "res/textured.vert")).attach(pgl::Shader(GL_FRAGMENT_SHADER, "res/textured.frag")).link()};
		return shaderProgram;
	}
	class imageLooper
	{
		std::vector<std::filesystem::path> paths;
		size_t iter = 0;

	public:
		imageLooper()
		{
			for(const auto& path : std::filesystem::directory_iterator("res/images"))
			{
				paths.emplace_back(path);
			}
		}
		const std::filesystem::path& getFile() const
		{
			return paths[iter];
		}
		imageLooper& operator++()
		{
			if(++iter == paths.size())
			{
				iter = 0;
			}
			return *this;
		}
		imageLooper& operator--()
		{
			if(iter == 0)
			{
				iter = paths.size() - 1;
			}
			else
			{
				--iter;
			}
			return *this;
		}
	} images;

public:
	picture(glm::vec2 pos, glm::vec2 size) :
		drawable(pos, size)
	{
		vao.bind();
		vao.bindEBO(ebo);
		vbo.bufferData(squareVertices, 0);
		ebo.bufferData(squareIndices, 0);
		getProgram().setAttrib<GLfloat>(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
		elements = squareIndices.size();
		loadNext();
	}
	void loadPicture(const std::string& path)
	{
		sf::Image image;
		image.loadFromFile(path.data());
		tex.load(GL_RGBA8, image.getSize().x, image.getSize().y, GL_RGBA, GL_UNSIGNED_BYTE, image.getPixelsPtr());
	}
	void draw() override
	{
		getProgram().use();
		tex.bind();
		glm::mat4 model(1);
		model = glm::translate(model, glm::vec3(pos, 0));
		model = glm::scale(model, glm::vec3(size, 0));
		glUniformMatrix4fv(0, 1, GL_FALSE, glm::value_ptr(model));
		drawable::draw();
	}
	void loadNext()
	{
		++images;
		loadPicture(images.getFile().string());
	}
	void loadPrevious()
	{
		--images;
		loadPicture(images.getFile().string());
	}
};

int main()
{
	sf::ContextSettings settings(24, 0, 0, 4, 5, sf::ContextSettings::Core);
#if NDEBUG
	settings.attributeFlags |= GL_CONTEXT_FLAG_NO_ERROR_BIT_KHR;
#else
	settings.attributeFlags |= sf::ContextSettings::Debug;
#endif
	sf::Window window(sf::VideoMode(800, 600), "buttons", sf::Style::Default, settings);
	window.setFramerateLimit(144);
	if(!gladLoadGL())
	{
		std::cerr << "Could not load GLAD\n";
		return 1;
	}
#ifndef NDEBUG
	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
#endif
	glDebugMessageCallback(pgl::MessageCallback, nullptr);
	pgl::storage::ubo ubo;
	ubo.bindBufferBase(0);
	ubo.reserve<glm::mat4>(2, GL_MAP_PERSISTENT_BIT | GL_MAP_WRITE_BIT | GL_MAP_COHERENT_BIT);
	glm::mat4* uboMappedPtr = static_cast<glm::mat4*>(ubo.getMappedPtr());
	uboMappedPtr[0] = glm::ortho<float>(0, 800, 600, 0, -1, 1);
	uboMappedPtr[1] = glm::translate(glm::mat4(1), glm::vec3(0, 0, -0.5f));
	std::vector<std::unique_ptr<drawable>> elements;
	picture& pictureElement = *static_cast<picture*>(elements.emplace_back(std::make_unique<picture>(glm::vec2{0, 0}, glm::vec2{800, 600})).get());
	elements.emplace_back(std::make_unique<button>(glm::vec2{750, 550}, glm::vec2{50, 50}, [&]() { pictureElement.loadNext(); }));
	elements.emplace_back(std::make_unique<button>(glm::vec2{0, 550}, glm::vec2{50, 50}, [&]() { pictureElement.loadPrevious(); }));
	interactable* lastClicked = nullptr;
	while(window.isOpen())
	{
		glClear(GL_COLOR_BUFFER_BIT);
		sf::Event event;
		if(!window.waitEvent(event))
		{
			return -1;
		}
		do
		{
			switch(event.type)
			{
				case sf::Event::Closed:
					window.close();
					return 0;
				case sf::Event::Resized:
					glViewport(0, 0, event.size.width, event.size.height);
					uboMappedPtr[0] = glm::ortho<float>(0, event.size.width, event.size.height, 0, -1, 1);
					break;
				case sf::Event::MouseMoved:
				{
					const auto mousepos = sf::Mouse::getPosition(window);
					for(auto& element : elements)
					{
						if(interactable* interElem = dynamic_cast<interactable*>(element.get()))
						{
							if(interElem->containsPoint(mousepos))
							{
								if(!interElem->clicked)
								{
									interElem->hovered = true;
									interElem->onHover();
								}
							}
							else if(interElem->hovered)
							{
								interElem->hovered = false;
								interElem->onLeave();
							}
						}
					}
				}
				break;
				case sf::Event::MouseButtonPressed:
				{
					if(event.mouseButton.button == sf::Mouse::Left)
					{
						const auto mousepos = sf::Mouse::getPosition(window);
						for(auto& element : elements)
						{
							if(interactable* interElem = dynamic_cast<interactable*>(element.get()))
							{
								if(interElem->containsPoint(mousepos))
								{
									lastClicked = interElem;
									interElem->clicked = true;
									interElem->onClick();
									break;
								}
							}
						}
					}
				}
				break;
				case sf::Event::MouseButtonReleased:
				{
					if(event.mouseButton.button == sf::Mouse::Left)
					{
						if(lastClicked)
						{
							lastClicked->clicked = false;
							lastClicked->onRelease();
							lastClicked = nullptr;
						}
					}
				}
				break;
				default:
					break;
			}
		} while(window.pollEvent(event));
		for(std::unique_ptr<drawable>& element : elements)
		{
			element->draw();
		}
		window.display();
	}
}
