#version 430 core

layout(location = 0) in vec2 pos;

layout(binding = 0) uniform MPVblock
{
	mat4 projection;
	mat4 view;
};

layout(location = 0) uniform mat4 model;

void main()
{
	gl_Position = projection * view * model * vec4(pos, 0, 1);
}
