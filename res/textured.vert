#version 430 core

layout(location = 0) in vec2 pos;
layout(location = 0) out vec2 texCoord;

layout(binding = 0) uniform MPVblock
{
	mat4 projection;
	mat4 view;
};

layout(location = 0) uniform mat4 model;

void main()
{
	texCoord = pos;
	gl_Position = projection * view * model * vec4(pos, 0, 1);
}
